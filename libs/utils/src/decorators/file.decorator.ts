import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Allow } from 'class-validator';
import { Readable } from 'stream';

export class FileData {
  @Allow()
  field: string;
  @Allow()
  file: Readable;
  @Allow()
  encoding: string;
  @Allow()
  mimetype: string;
  @Allow()
  filename: string;
}

export const FileBody = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    return request.body;
  },
);
