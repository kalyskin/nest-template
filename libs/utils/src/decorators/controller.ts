import { applyDecorators, Controller, SetMetadata } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

export function AdminController(name: string, version: string) {
  return applyDecorators(
    SetMetadata('admin_controller', true),
    ApiTags(`${name}-admin-${version}`),
    Controller(version),
  );
}

export function MobileController(name: string, version: string) {
  return applyDecorators(
    SetMetadata('mobile_controller', true),
    ApiTags(`${name}-mobile-${version}`),
    Controller(version),
  );
}
