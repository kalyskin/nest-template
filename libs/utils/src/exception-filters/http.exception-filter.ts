import {
  BadRequestException,
  ConflictException,
  Exception,
  ForbiddenException,
  NotFoundException,
  UnauthorizedException,
} from '@app/utils/base-exception';
import { DbException } from '@app/utils/db-exception';
import { ErrorHttpResponse } from '@app/utils/response';
import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { HttpRawResponse } from '../http';

const getStatusCode = (exception: Exception) => {
  if (exception instanceof BadRequestException) {
    return 400;
  } else if (exception instanceof UnauthorizedException) {
    return 401;
  } else if (exception instanceof ForbiddenException) {
    return 403;
  } else if (exception instanceof NotFoundException) {
    return 404;
  } else if (exception instanceof ConflictException) {
    return 409;
  }
  return 500;
};

@Catch(Exception)
export class HttpExceptionFilter implements ExceptionFilter {
  catch(exception: Exception, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<HttpRawResponse>();
    const request = ctx.getRequest<Request>();
    const errorCode = exception.status;
    const statusCode = getStatusCode(exception);

    response.code(statusCode).send(
      new ErrorHttpResponse({
        message: exception.message,
        data: exception.data,
        errorCode,
        timestamp: new Date().toISOString(),
        path: request.url,
      }),
    );
  }
}

// in case some db exception goes through manual handling
@Catch(DbException)
export class HttpDbExceptionFilter implements ExceptionFilter {
  catch(exception: DbException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<HttpRawResponse>();
    const request = ctx.getRequest<Request>();
    const errorCode = 'UKNOWN_ERROR';
    const message = 'Some unknown error has occurred';

    console.error("A db exception wasn't handled", exception);

    response.code(500).send(
      new ErrorHttpResponse({
        message,
        errorCode,
        timestamp: new Date().toISOString(),
        path: request.url,
      }),
    );
  }
}
