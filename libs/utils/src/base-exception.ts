import { ApiProperty } from '@nestjs/swagger';

export interface IException {
  readonly status: string;
  readonly message: string;
  readonly data?: any;
}

export class Exception extends Error implements IException {
  @ApiProperty()
  readonly status: string;
  @ApiProperty()
  readonly message: string;
  @ApiProperty()
  readonly data?: any;
  constructor(message: string, data?: any, status?: string) {
    super(message);
    console.error(
      `Error was created: ${message} ${JSON.stringify(data)} ${status}`,
    );
    this.data = data;
    this.status = status || 'UNKNOWN_EXCEPTION';
    Object.setPrototypeOf(this, Exception.prototype);
  }
}

export class BadRequestException extends Exception {
  constructor(message: string, data?: any, status?: string) {
    super(message, data, status);
    Object.setPrototypeOf(this, BadRequestException.prototype);
  }
}

export class NotFoundException extends Exception {
  constructor(message: string, data?: any) {
    super(message, data);
    Object.setPrototypeOf(this, NotFoundException.prototype);
  }
}

export class ForbiddenException extends Exception {
  constructor(message: string, data?: any) {
    super(message, data);
    Object.setPrototypeOf(this, ForbiddenException.prototype);
  }
}

export class UnauthorizedException extends Exception {
  constructor(message: string, data?: any) {
    super(message, data);
    Object.setPrototypeOf(this, UnauthorizedException.prototype);
  }
}

export class ConflictException extends Exception {
  constructor(message: string, data?: any) {
    super(message, data);
    Object.setPrototypeOf(this, ConflictException.prototype);
  }
}
