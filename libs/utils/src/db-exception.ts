export enum PgCodes {
  UNIQUE_VIOLATION = '23505',
  NOT_NULL_VIOLATION = '23502',
  FOREIGN_VIOLATION = '23503',
}

export enum DbExceptionStatus {
  UNIQUE = 'UNIQUE',
  NOT_NULL = 'NOT_NULL',
  FOREIGN_KEY = 'FOREIGN_KEY',
  UNKNOWN = 'UNKNOWN',
}

export const PgToDbStatusMap = {
  [PgCodes.UNIQUE_VIOLATION]: DbExceptionStatus.UNIQUE,
  [PgCodes.NOT_NULL_VIOLATION]: DbExceptionStatus.NOT_NULL,
  [PgCodes.FOREIGN_VIOLATION]: DbExceptionStatus.FOREIGN_KEY,
};

export class DbException extends Error {
  constructor(
    readonly status: DbExceptionStatus,
    readonly originalError: Error,
  ) {
    super(status);
    Object.setPrototypeOf(this, DbException.prototype);
  }
}

export const createDbException = (pgError: any) => {
  const status: DbExceptionStatus =
    PgToDbStatusMap[pgError.code] || DbExceptionStatus.UNKNOWN;
  if (status === DbExceptionStatus.UNKNOWN) {
    console.error('UNEXPECTED DB ERROR:', pgError.message);
  }
  return new DbException(status, pgError);
};

export const ignoreUniqueException = (err: Error) => {
  if (
    !(err instanceof DbException) ||
    err.status !== DbExceptionStatus.UNIQUE
  ) {
    throw err;
  }
};
