const capitalize = (str: string) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
};

export const snakeToCamelCase = (str: string): string => {
  const split = str.split('_');
  return split
    .map((value: string, index: number) => {
      if (index === 0) {
        return value;
      }
      return capitalize(value);
    })
    .join('');
};

export const camelToSnakeCase = (str) => {
  const withUnderscores = str.replace(/([A-Z])/g, '_$1');
  return withUnderscores.toLowerCase();
};
/**
 * Changes the props of an object from snake case to camel case
 * IMPORTANT: modifies source object
 * @param item: object to snakify
 */
export const camelizeObject = (item) => {
  for (const prop in item) {
    const camel = snakeToCamelCase(prop);
    if (!(camel in item)) {
      item[camel] = item[prop];
      delete item[prop];
    }
  }
  return item;
};
