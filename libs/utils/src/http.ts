import { FastifyReply, FastifyRequest } from 'fastify';

export type HttpRawResponse = FastifyReply<any>;

export type HttpRawRequest = FastifyRequest<any>;
