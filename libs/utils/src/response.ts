export class HttpResponse<T, E> {
  status: string;
  data?: T;
  error?: E;
  constructor(status: 'OK' | 'ERROR', data: T, error?: E) {
    this.status = status;
    this.data = data;
    this.error = error;
  }
}

export class SuccessHttpResponse<T> extends HttpResponse<T, null> {
  constructor(readonly data: T) {
    super('OK', data);
  }
}

export interface ErrorData {
  message: string;
  errorCode: string;
  timestamp: string;
  path: string;
  data?: any;
}

export class ErrorHttpResponse extends HttpResponse<null, ErrorData> {
  constructor(readonly error: ErrorData) {
    super('ERROR', null, error);
  }
}
