import { ValidationError as NestValidationError } from '@nestjs/common';
import { BadRequestException } from './base-exception';

class ValidationError extends BadRequestException {
  readonly status = 'VALIDATION_ERROR';

  constructor(data: any) {
    super('An error occurred during validation', data);
  }
}

export const transformValidationErrorToBaseException = (
  errors: NestValidationError[],
) => {
  throw new ValidationError({ validationErrors: errors });
};
