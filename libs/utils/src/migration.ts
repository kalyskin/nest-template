import * as DBMigrate from 'db-migrate';

export async function migrate(config) {
  const dbMigrate = DBMigrate.getInstance(true, config);
  return dbMigrate.up();
}
