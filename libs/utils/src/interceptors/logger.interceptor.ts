import { HttpRawRequest } from '@app/utils/http';
import {
  CallHandler,
  ExecutionContext,
  Injectable,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  private readonly logger = new Logger('HTTP_LOGGER');

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const req: HttpRawRequest = context.switchToHttp().getRequest();
    this.logger.verbose(
      `REQ: ${process.pid} ${req.id} ${req.raw.method} ${
        req.raw.url
      } ${JSON.stringify(req.body)}`,
    );

    return next
      .handle()
      .pipe(
        tap((data) =>
          this.logger.verbose(
            `RES: ${process.pid} ${req.id} ${req.raw.method} ${
              req.raw.url
            } ${JSON.stringify(data)}`,
          ),
        ),
      );
  }
}
