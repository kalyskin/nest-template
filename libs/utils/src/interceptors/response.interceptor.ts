import { SuccessHttpResponse } from '@app/utils/response';
import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ResponseInterceptor<T>
  implements NestInterceptor<T, SuccessHttpResponse<T>> {
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<SuccessHttpResponse<T>> {
    const req = context.switchToHttp().getRequest();
    const contentType = req.headers['content-type'];
    const accept = req.headers['accept'];
    const isJson = [contentType, accept].some(
      (it) => it && it.includes('application/json'),
    );
    return next
      .handle()
      .pipe(map((data) => (isJson ? new SuccessHttpResponse(data) : data)));
  }
}
