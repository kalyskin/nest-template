import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { BadRequestException } from '@app/utils/base-exception';

@Injectable()
export class FileInterceptor implements NestInterceptor {
  async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<any>> {
    const req: any = context.switchToHttp().getRequest();
    if (!req.isMultipart()) {
      throw new BadRequestException('Request is not multipart');
    }
    await new Promise((resolve) => {
      const uploadHandler = async (
        field,
        file,
        filename,
        encoding,
        mimetype,
      ) => {
        req.body = {
          file,
          filename,
          encoding,
          mimetype,
          field,
        };
        resolve(null);
      };
      req.multipart(uploadHandler, () => {
        return;
      });
    });

    return next.handle();
  }
}
