import { Global, Logger, Module, OnApplicationBootstrap } from '@nestjs/common';
import { ConfigService } from './config.service';
import * as path from 'path';
import { ConfigManagerModule } from '@nestjsplus/config';

@Global()
@Module({
  imports: [
    ConfigManagerModule.register({
      useEnv: true,
      useFunction: (rootFolder, environment) => {
        console.log({ rootFolder, environment });
        return path.join(
          rootFolder,
          'config',
          environment,
          `${environment}.env`,
        );
      },
      defaultEnvironment: 'development',
      allowExtras: true,
    }),
  ],
  exports: [ConfigService],
  providers: [ConfigService],
})
export class ConfigModule implements OnApplicationBootstrap {
  onApplicationBootstrap(): any {
    const logger = new Logger('ConfigModuleBootstrap');
    logger.log('Test');
  }
}
