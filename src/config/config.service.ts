import { Injectable } from '@nestjs/common';
import * as Joi from '@hapi/joi';
import { ConfigManager } from '@nestjsplus/config';
import { KnexOptions, KnexOptionsFactory } from '@nestjsplus/knex';
import { JwtModuleOptions, JwtOptionsFactory } from '@nestjs/jwt';
import { camelizeObject, camelToSnakeCase } from '@app/utils/words-case';

@Injectable()
export class ConfigService
  extends ConfigManager
  implements KnexOptionsFactory, JwtOptionsFactory {
  createJwtOptions(): JwtModuleOptions {
    return {
      secret: this.get<string>('JWT_SECRET'),
      signOptions: {
        expiresIn: '30 days',
      },
    };
  }
  createKnexOptions(): KnexOptions {
    return {
      client: 'pg',
      debug: false,
      postProcessResponse: (result) => {
        if (Array.isArray(result)) {
          return result.map((row) => camelizeObject(row));
        } else {
          return camelizeObject(result);
        }
      },
      wrapIdentifier: (value, origImpl) => origImpl(camelToSnakeCase(value)),
      connection: {
        host: this.get<string>('DB_HOST'),
        user: this.get<string>('DB_USERNAME'),
        password: this.get<string>('DB_PASSWORD'),
        database: this.get<string>('DB_NAME'),
        port: this.get<number>('DB_PORT'),
      },
    };
  }

  provideConfigSpec() {
    return {
      CORS_ORIGIN: {
        validate: Joi.exist(),
        required: true,
      },
      SWAGGER: {
        validate: Joi.exist(),
        required: true,
      },
      DB_HOST: {
        validate: Joi.exist(),
        required: true,
      },
      DB_PORT: {
        validate: Joi.exist(),
        required: true,
      },
      DB_USERNAME: {
        validate: Joi.exist(),
        required: true,
      },
      DB_PASSWORD: {
        validate: Joi.exist(),
        required: true,
      },
      DB_NAME: {
        validate: Joi.exist(),
        required: true,
      },
      JWT_SECRET: {
        validate: Joi.exist(),
        required: true,
      },
    };
  }

  getCorsOrigin(): boolean | string {
    const value = this.get<string>('CORS_ORIGIN');
    if (['true', 'false'].includes(value)) {
      return value === 'true';
    }
    return value;
  }

  getIsSwaggerOn(): boolean {
    const value = this.get<string>('SWAGGER');
    return value === 'true';
  }

  getMigrateOptions() {
    return {
      config: {
        dev: {
          driver: 'pg',
          user: this.get<string>('DB_USERNAME'),
          password: this.get<string>('DB_PASSWORD'),
          host: this.get<string>('DB_HOST'),
          database: this.get<string>('DB_NAME'),
          port: this.get<number>('DB_PORT'),
        },
      },
    };
  }
}
