import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { LoggingInterceptor } from '@app/utils/interceptors/logger.interceptor';
import { ConfigService } from './config/config.service';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { transformValidationErrorToBaseException } from '@app/utils/validation-pipe';
import { ResponseInterceptor } from '@app/utils/interceptors/response.interceptor';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import {
  HttpDbExceptionFilter,
  HttpExceptionFilter,
} from '@app/utils/exception-filters/http.exception-filter';
import { migrate } from '@app/utils/migration';

async function bootstrap() {
  const fastifyAdapter = new FastifyAdapter();
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    fastifyAdapter,
  );
  const configService = app.get(ConfigService);
  app.enableCors({ origin: configService.getCorsOrigin() });

  if (configService.getIsSwaggerOn()) {
    const swaggerOptions = new DocumentBuilder()
      .setTitle('NestJS')
      .setDescription('The API description')
      .setVersion('1.0')
      .addTag('NestJS')
      .build();
    const document = SwaggerModule.createDocument(app, swaggerOptions);
    SwaggerModule.setup('api', app, document);
  }

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      exceptionFactory: transformValidationErrorToBaseException,
    }),
  );
  app.useGlobalInterceptors(
    new LoggingInterceptor(),
    new ResponseInterceptor(),
  );
  app.useGlobalFilters(new HttpExceptionFilter(), new HttpDbExceptionFilter());

  migrate(configService.getMigrateOptions()).then(() => app.listen(4000));
}

bootstrap();
