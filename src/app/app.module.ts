import { Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { ConfigModule } from '../config/config.module';
import { RouterModule } from 'nest-router';
import { routes } from './routes';
import { ServicesModule } from '../services/services.module';

@Module({
  imports: [
    RouterModule.forRoutes(routes),
    ConfigModule,
    UserModule,
    ServicesModule,
  ],
})
export class AppModule {}
