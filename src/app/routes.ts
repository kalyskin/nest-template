import { Routes } from 'nest-router';
import { UserModule } from '../user/user.module';
import { ServicesModule } from '../services/services.module';
import { UserAdminModule } from '../user/admin/admin.module';
import { ServicesAdminModule } from '../services/admin/admin.module';

export const routes: Routes = [
  {
    path: 'kyc',
    module: UserModule,
  },
  {
    path: 'services',
    module: ServicesModule,
  },
  {
    path: 'kyc-admin',
    module: UserAdminModule,
  },
  {
    path: 'services-admin',
    module: ServicesAdminModule,
  },
];
