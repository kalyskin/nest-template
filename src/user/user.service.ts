import { Injectable } from '@nestjs/common';

@Injectable()
export class UserService {
  index() {
    return { message: 'Index page' };
  }
  getHello() {
    return { message: 'Hello World!' };
  }

  getName() {
    return { name: 'Kalys' };
  }
}
