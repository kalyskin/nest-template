import { Module } from '@nestjs/common';
import { UserAdminController } from './admin.controller';

@Module({
  controllers: [UserAdminController],
})
export class UserAdminModule {}
