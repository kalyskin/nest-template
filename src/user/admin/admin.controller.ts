import { Get } from '@nestjs/common';
import { V1 } from '@app/utils/routes';
import { AdminController } from '@app/utils/decorators/controller';

@AdminController('kyc', V1)
export class UserAdminController {
  @Get('/auth')
  getHello() {
    return { message: 'Hello' };
  }

  @Get('/name')
  getName() {
    return { name: 'Kalys' };
  }
}
