import { Get } from '@nestjs/common';
import { UserService } from './user.service';
import { MobileController } from '@app/utils/decorators/controller';
import { V1 } from '@app/utils/routes';

@MobileController('kyc', V1)
export class UserController {
  constructor(private readonly appService: UserService) {}

  @Get()
  getHello() {
    return this.appService.index();
  }
}
