import { Module } from '@nestjs/common';
import { ServicesService } from './services.service';
import { ServicesController } from './services.controller';
import { ServicesAdminModule } from './admin/admin.module';

@Module({
  providers: [ServicesService],
  controllers: [ServicesController],
  imports: [ServicesAdminModule],
})
export class ServicesModule {}
