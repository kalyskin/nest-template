import { Injectable } from '@nestjs/common';

@Injectable()
export class ServicesService {
  getHello() {
    return { message: 'Hello World!' };
  }

  getName() {
    return { name: 'Kalys' };
  }
}
