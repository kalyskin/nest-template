import { Get } from '@nestjs/common';
import { ServicesService } from './services.service';
import { MobileController } from '@app/utils/decorators/controller';
import { V1 } from '@app/utils/routes';

@MobileController('services', V1)
export class ServicesController {
  constructor(private readonly servicesService: ServicesService) {}

  @Get('/hello')
  getHello() {
    return this.servicesService.getHello();
  }

  @Get('/name')
  getName() {
    return this.servicesService.getName();
  }
}
