import { AdminController } from '@app/utils/decorators/controller';
import { V1 } from '@app/utils/routes';
import { Get } from '@nestjs/common';

@AdminController('services', V1)
export class ServicesAdminController {
  @Get('/method1')
  method1() {
    return { message: 'method1' };
  }

  @Get('/method2')
  method2() {
    return { message: 'method2' };
  }
}
