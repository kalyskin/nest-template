import { Module } from '@nestjs/common';
import { ServicesAdminController } from './admin.controller';

@Module({
  controllers: [ServicesAdminController],
})
export class ServicesAdminModule {}
